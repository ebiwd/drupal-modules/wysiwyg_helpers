<?php
// $Id$

/**
 * @file
 * Change button ordering for the WYSIWYG module.
 */

define ('WYSIWYG_TOOLBAR_ENABLED', TRUE);
define ('WYSIWYG_TOOLBAR_TOOLBAR_1', 'bold,italic,underline,strikethrough,ins,del,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,|,bullist,numlist,|,link,linkit,unlink,image,media,anchor,|,hr,advhr,sub,sup,charmap,emotions,|,ltr,rtl,|,insertlayer,moveforward,movebackward,|,absolute');
define ('WYSIWYG_TOOLBAR_TOOLBAR_2', 'formatselect,fontselect,fontsizeselect,styleselect,removeformat,cleanup,|,forecolor,backcolor,|,insertdate,inserttime,|,blockquote,cite,abbr,acronym,|,undo,redo,|,cut,copy,paste,pastetext,pasteword,selectall,search,replace,|,code,advcode,styleprops,attribs,preview,visualaid,help,print,fullscreen');
define ('WYSIWYG_TOOLBAR_TOOLBAR_3', 'tablecontrols,|,break,|,template');
 
/**
 * Implementation of hook_menu().
 */
function wysiwyg_toolbar_menu() {
  $items['admin/settings/wysiwyg/toolbar'] = array(
    'title' => 'Toolbar Designer',
    'description' => 'Designer for WYSIWYG Toolbar.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wysiwyg_toolbar_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer wysiwyg toolbar settings'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );

  return $items;
}

/**
 * Implementation of hook_settings().
 */
function wysiwyg_toolbar_settings() {
  $form['wysiwyg_styles_settings']['wysiwyg_toolbar_advice'] = array(
    '#type'          => 'markup',
    '#value'         => t('<p>Toolbar Designer allows you to define the button order for WYSIWYG TinyMCE editor</p>'),
  );
  $form['wysiwyg_stlyes_settings']['wysiwyg_toolbar_enabled'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable Toolbar Designer'),
    '#default_value' => variable_get('wysiwyg_toolbar_enabled', WYSIWYG_TOOLBAR_ENABLED),
    '#description'   => t('Check box to enable WYSIWYG Buttons.'),
  );
  $form['wysiwyg_stlyes_settings']['wysiwyg_toolbar_toolbar_1'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Toolbar 1'),
    '#default_value' => variable_get('wysiwyg_toolbar_toolbar_1', WYSIWYG_TOOLBAR_TOOLBAR_1),
    '#description'   => t(''),
  );
  $form['wysiwyg_stlyes_settings']['wysiwyg_toolbar_toolbar_2'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Toolbar 2'),
    '#default_value' => variable_get('wysiwyg_toolbar_toolbar_2', WYSIWYG_TOOLBAR_TOOLBAR_2),
    '#description'   => t(''),
  );
  $form['wysiwyg_stlyes_settings']['wysiwyg_toolbar_toolbar_3'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Toolbar 3'),
    '#default_value' => variable_get('wysiwyg_toolbar_toolbar_3', WYSIWYG_TOOLBAR_TOOLBAR_3),
    '#description'   => t(''),
  );
  
  return system_settings_form($form);
}

/**
 * Implementation of hook_wysiwyg_editot_settings_alter().
 */
function wysiwyg_toolbar_wysiwyg_editor_settings_alter(&$settings, $context) {
  $wysiwyg_toolbar_enabled = variable_get('wysiwyg_toolbar_enabled', WYSIWYG_TOOLBAR_ENABLED);

  if ($context['profile']->editor == 'tinymce' && $wysiwyg_toolbar_enabled) {
    $wysiwyg_toolbar_toolbar['1']= explode(',', variable_get('wysiwyg_toolbar_toolbar_1', WYSIWYG_TOOLBAR_TOOLBAR_1));
    $wysiwyg_toolbar_toolbar['2'] = explode(',', variable_get('wysiwyg_toolbar_toolbar_2', WYSIWYG_TOOLBAR_TOOLBAR_2));
    $wysiwyg_toolbar_toolbar['3'] = explode(',', variable_get('wysiwyg_toolbar_toolbar_3', WYSIWYG_TOOLBAR_TOOLBAR_3));

    drupal_add_css(drupal_get_path('module', 'wysiwyg_toolbar') . '/wysiwyg_toolbar.css');

    // get list of enabled plugins
    $plugins = wysiwyg_get_plugins($context['profile']->editor);
    foreach ($plugins as $name => $plugin) {
      if (isset($plugin['buttons'])) {
        foreach ($plugin['buttons'] as $button => $button_name) {
          $enabled_buttons[$button] = !empty($context['profile']->settings['buttons'][$name][$button]) ? $context['profile']->settings['buttons'][$name][$button] : FALSE;
        }
      }
    }
    
    // create grouped toolbars according to enabled buttons
    foreach ($wysiwyg_toolbar_toolbar as $index => $toolbar) {
      $theme_advanced_toolbar[$index] = '';
      foreach ($toolbar as $button) {
        $button = trim($button);
        if ($button == 'separator') $button = '|'; 

        if ($button == '|' || (isset($enabled_buttons[$button]) && $enabled_buttons[$button])) {
          $theme_advanced_toolbar[$index] .= $button . ',';
        }
        unset($enabled_buttons[$button]);
      }

      $theme_advanced_toolbar[$index] = preg_replace('/\|[\|,]*/', '|,', $theme_advanced_toolbar[$index]);
      $theme_advanced_toolbar[$index] = preg_replace('/[\|,]*$/', '', $theme_advanced_toolbar[$index]);
      $theme_advanced_toolbar[$index] = preg_replace('/^[\|,]*/', '', $theme_advanced_toolbar[$index]);
    }

    foreach ($enabled_buttons as $button => $enabled) {
      if ($enabled) {
        $unplaced_buttons[] = $button;
      }
    }
    if (isset($unplaced_buttons)) {
      $theme_advanced_toolbar['3'] .= '|,' . join(',', $unplaced_buttons);
    }

    $settings['theme_advanced_buttons1'] = $theme_advanced_toolbar['1'];
    $settings['theme_advanced_buttons2'] = $theme_advanced_toolbar['2'];
    $settings['theme_advanced_buttons3'] = $theme_advanced_toolbar['3'];
  }
}
