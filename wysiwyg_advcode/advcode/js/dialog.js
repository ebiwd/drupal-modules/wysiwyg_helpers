tinyMCEPopup.onInit.add(onLoadInit);

function saveContent() {
  tinyMCEPopup.editor.setContent(editor.getCode(), {source_view : true});
  tinyMCEPopup.close();
}
/*
function saveContent() {
	tinyMCEPopup.editor.setContent(document.getElementById('htmlSource').value, {source_view : true});
	tinyMCEPopup.close();
}
*/

function onLoadInit() {
	tinyMCEPopup.resizeToInnerSize();
  
	// Remove Gecko spellchecking
	if (tinymce.isGecko)
		document.body.spellcheck = tinyMCEPopup.editor.getParam("gecko_spellcheck");

  setTimeout(function() {
    window.editor = new CodeMirror(CodeMirror.replace('htmlSource'), {
      path: libraries['codemirror'] + "/js/",
      parserfile: ["parsexml.js", "parsecss.js", "tokenizejavascript.js", "parsejavascript.js", "parsehtmlmixed.js"],
      stylesheet: [libraries['codemirror'] + "/css/xmlcolors.css", libraries['codemirror'] + "/css/jscolors.css", libraries['codemirror'] + "/css/csscolors.css"],      
      height: "90%",
      reindentOnLoad: true,
      lineNumbers: true,
      content: tinyMCEPopup.editor.getContent()
    });
    window.focus();
  }, 0);
  /*
	document.getElementById('htmlSource').value = tinyMCEPopup.editor.getContent({source_view : true});
*/

	if (tinyMCEPopup.editor.getParam("theme_advanced_source_editor_wrap", true)) {
		setWrap('soft');
	}

	resizeInputs();
}

function setWrap(val) {
	var v, n, s = document.getElementById('htmlSource');

	s.wrap = val;

	if (!tinymce.isIE) {
		v = s.value;
		n = s.cloneNode(false);
		n.setAttribute("wrap", val);
		s.parentNode.replaceChild(n, s);
		n.value = v;
	}
}

function toggleWordWrap(elm) {
	if (elm.checked)
		setWrap('soft');
	else
		setWrap('off');
}

function resizeInputs() {
	var vp = tinyMCEPopup.dom.getViewPort(window), el;

	el = document.getElementById('htmlSource');

	if (el) {
		el.style.width  = (vp.w - 20) + 'px';
		el.style.height = (vp.h - 65) + 'px';
	}
}

function tidy() {
  editor.setCode(editor.getCode().replace(/\n/g, "{preserve_newline}"));
  
  // replace multiple spaces with single space
  editor.setCode(editor.getCode().replace(/\s+/g, " "));

  // compact block level html tags, followed by CR
  editor.setCode(editor.getCode().replace(/\s*(<\/?(div|h[1-6]|p|[uod]l|d[td]|table|t(head|body|foot)|tr|blockquote|q|pre|form)[^>]*>)\s*/g, "\n$1\n"));

  // insert CR after br and /tr, /th, /li
  editor.setCode(editor.getCode().replace(/(<br[^>]*>)/g, "$1\n").replace(/(<\/(t[hd]|li)[^>]*>)/g, "$1\n"));

  // remove double spacing
  editor.setCode(editor.getCode().replace(/\n+/g, "\n"));

  editor.setCode(editor.getCode().replace(/\n*{preserve_newline}\n*/g, "\n"));

  // remove head and foot newline
  editor.setCode(editor.getCode().replace(/(^\n)|(\n$)/g, ""));

  // reindent
  editor.reindent();
}
