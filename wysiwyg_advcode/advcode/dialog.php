<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Edit HTML Source</title>
	<script type="text/javascript">
    var libraries = [];
    libraries['codemirror'] = '<?php echo $codemirror = $_GET['codemirror']; ?>'; 
    libraries['tinymce'] = '<?php echo $tinymce = $_GET['tinymce']; ?>';
  </script>
  <script type="text/javascript" src="<?php echo $codemirror; ?>/js/codemirror.js"></script>
  <script type="text/javascript" src="<?php echo $tinymce; ?>/jscripts/tiny_mce/tiny_mce_popup.js"></script>
	<script type="text/javascript" src="js/dialog.js"></script>
  <link type="text/css" rel="stylesheet" media="all" href="css/dialog.css" /> 
</head>
<body onresize="resizeInputs();" style="display:none; overflow:hidden;">
	<form name="source" onsubmit="saveContent();return false;" action="#">
		<div style="float: left" class="title">Edit HTML Source</div>
  
    <div style="float:right">
      <a href="#" onclick="tidy(); return false;">Tidy</a>
    </div>
    
    <br style="clear: both;" />
    
		<textarea name="htmlSource" id="htmlSource" rows="15" cols="100" style="width: 100%; height: 100%; font-family: 'Courier New',Courier,monospace; font-size: 12px;" dir="ltr" wrap="off" class="mceFocus"></textarea>

		<div class="mceActionPanel">
			<input type="submit" name="insert" value="Update" id="insert" />
			<input type="button" name="cancel" value="Cancel" onclick="tinyMCEPopup.close();" id="cancel" />
		</div>
	</form>
</body>
</html>
