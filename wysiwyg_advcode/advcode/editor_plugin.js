(function() {
  tinymce.create('tinymce.plugins.advcode', {
    init : function(ed, url) {
      // Register commands
      ed.addCommand('mceAdvancedCode', function() {
        ed.windowManager.open({
          file : url + '/dialog.php?codemirror=' + Drupal.settings.advcode.codemirror + '&tinymce=' + Drupal.settings.advcode.tinymce,
          width : 750 + parseInt(ed.getLang('advcode.delta_width', 0)),
          height : 450 + parseInt(ed.getLang('advcode.delta_height', 0)),
          inline : 1
        }, {
          plugin_url : url
        });
      });

      // Register buttons
      ed.addButton('advcode', {
        title : ed.getLang('advcode.desc', 0),
        cmd : 'mceAdvancedCode',
        image : url + '/img/html.gif'
      });

      ed.onNodeChange.add(function(ed, cm, n) {});
    },

    getInfo : function() {
      return {
        longname : 'Advanced Code Editor using CodeMirror',
        author : 'Peter Walter',
        authorurl : 'http://www.ebi.ac.uk',
        version : tinymce.majorVersion + "." + tinymce.minorVersion
      };
    }
  });

  // Register plugin
  tinymce.PluginManager.add('advcode', tinymce.plugins.advcode);
})();
